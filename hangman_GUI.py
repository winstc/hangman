# Written By: Winston Cadwell on 11/20/2016
# Project Name: Hangman Game
# Description: This is a GUI hangman game. The user is presented with blank spaces and is
#              asked to guess the word. Word are loaded from lists in the "lists" directory.

import tkinter as t  # import tkinter as t
import random as r  # import random library
import functools as ft  # import function tools
from configparser import ConfigParser  # import configuration parser
import os  # import os libraries


class Hangman_GUI():  # class for GUI Hangman
    def __init__(self):  # init method
        self.scale_factor = 1  # stores the desired sale factor for the program, default is 1
        self.font_size = 8  # stores desired font size for program, default is 8

        self.c = ConfigParser()  # create a new config parser called 'c'

        self.read_config()  # call read_config to read the config.ini file

        self.selected_list = ""  # string to hold the users list choice
        self.current_word = ""  # string to hold the goal word
        self.wrong_letters = ""  # string to hold incorrect letters
        self.correct_letters = ""  # string to hold correct letters
        self.lives = 6  # stores the number of lives the player has left
        self.letters_left = 0  # stores the number of letters the user still need to guess
        self.score = 0  # stores the users score

        self.gameEnabled = False  # set if the user can play

        self.root = t.Tk()  # create new tkinter instance, root window
        self.root.tk.call('tk', 'scaling', self.scale_factor)  # set the scaling factor
        win_size= '{}x{}'.format(self.scale_factor*400, self.scale_factor*415)  # generate window size according to scale factor
        self.root.geometry(win_size)  # set window size
        self.root.title("Hangman")  # change window title

        self.menubar = t.Menu(self.root)  # create a new menu bar
        self.gamemenu = t.Menu(self.menubar)  # create a sub-menu
        self.menubar.add_cascade(label="Game", font=self.font_size, menu=self.gamemenu)  # add game menu to menubar as cascade
        self.root.config(menu=self.menubar)  # set menubar as the menu bar for root window

        self.gamemenu.add_command(label="New Word", font=self.font_size, command=self.new_word)  # add New Word Command to gamemenu
        self.gamemenu.add_command(label="Quit", font=self.font_size, command=self.root.quit)  # add Quit option to gamemenu

        self.opt1_val = t.StringVar()  # create a variable to hold selected value of list selection menu

        self.list_dict = {}  # create a dictionary for storing available word lists
        count = 1  # counter variable used to index lists

        options = []  # store option so they can be added to list selection menu

        for foldername in os.listdir('./lists'):  # for each folder in the list directory
            self.c.read("./lists/{}/list.ini".format(foldername))  # open its list.ini file with config parser
            options.append(self.c.get('main', 'name'))  # from section main read name, append it to options list
            self.list_dict[count] = str(self.c.get('main', 'name'))  # add the list name to list dictionary
            count += 1  # increment counter by 1

        opt1 = t.OptionMenu(self.root, self.opt1_val, *options, command=self.change_list)  # create a new option menu, use options var as items
        opt1.config(font=self.font_size)  # change the font size of the option menu
        opt1.pack(fill="x")  # add the option menu to the window

        self.can = t.Canvas(self.root, width=400*self.scale_factor, height=350*self.scale_factor)  # Create a tk canvas, set its size
        self.can.pack(fill="both", expand=True)  # add canvas to window

        self.draw_man(6)  # draw the hangman at lives=6
        self.update_letter_banks()  # draw the letter blanks

        self.frame1 = t.Frame(self.root, width=400*self.scale_factor)  # create a new frame
        self.frame1.pack(side="bottom", anchor="s", fill="x")  # add frame to the window

        self.guessBox = t.Entry(self.frame1, font=self.font_size)  # create entry box for user input
        self.guessBox.bind("<Return>", ft.partial(self.check_guess))  # bind return key to check_guess function
        self.guessBox.pack(anchor="se", side="left", fill="x", expand=True, pady=5)  # add entry to window

        self.guessButton = t.Button(self.frame1, text="GUESS", font=self.font_size)  # create guess button
        self.guessButton.bind("<Button-1>", ft.partial(self.check_guess))  # bind mouse 1 click to check_guess function
        self.guessButton.pack(anchor="sw", side="right")  # add it to the window

        self.guessBox.focus_set()  # give the guess box focus so the use can type as soon as they start the program

        self.root.mainloop()  # enter the tk mainloop

    def read_config(self):  # reads the config.ini file
        self.c.read("./config.ini")  # tell config parser to read config.ini
        try: # try the following
            self.scale_factor = int(self.c.get('UI', 'scale_factor'))  # set scale factor to the value in conf file
            self.font_size = int(self.c.get('UI', 'font_size'))  # set font size to value in conf file
        except ValueError: # if something goes wrong
            print("Error parsing config file!")  # print error parsing file

    def draw_man(self, lives):  # draws hangman

        self.can.delete("all")  # clear the canvas

        sf = self.scale_factor  # assign a shorter name to scale_factor for readability

        # draw gallows
        self.can.create_line(50*sf, 200*sf, 125*sf, 200*sf, width=3.5*sf)  # base
        self.can.create_line(75*sf, 200*sf, 75*sf, 50*sf, width=3.5*sf)  # upright
        self.can.create_line(75*sf, 162.5*sf, 100*sf, 200*sf, width=3.5*sf)  # diagonal
        self.can.create_line(75*sf, 50*sf, 125*sf, 50*sf, width=3.5*sf)  # upper cross
        self.can.create_line(125*sf, 50*sf, 125*sf, 75*sf, width=3.5*sf)  # upper down

        # draw the man
        if lives <= 5:
            self.can.create_oval(112.5*sf, 75*sf, 137.5*sf, 100*sf, width=3.5*sf)  # head
        if lives <= 4:
            self.can.create_line(125*sf, 100*sf, 125*sf, 150*sf, width=3.5*sf)  # body
        if lives <= 3:
            self.can.create_line(125*sf, 112.5*sf, 105*sf, 130*sf, width=3.5*sf)  # left arm
        if lives <= 2:
            self.can.create_line(125*sf, 112.5*sf, 140*sf, 130*sf, width=3.5*sf)  # right arm
        if lives <= 1:
            self.can.create_line(125*sf, 150*sf, 105*sf, 167.5*sf, width=3.5*sf)  # left leg
        if lives <= 0:
            self.can.create_line(125*sf, 150*sf, 140*sf, 167.5*sf, width=3.5*sf)  # right leg

    def update_letter_banks(self):  # updates letter banks

        sf = self.scale_factor  # assign a shorter name to scale_factor for readability

        self.letters_left = len(self.current_word)  # set letters left to length of current word

        self.can.create_text(260*sf, 49*sf, text="Wrong Letters:", font=self.font_size)  # add text "Wrong letters to canvas
        # draw a box for wrong letters
        self.can.create_line(210*sf, 62.5*sf, 210*sf, 212.5*sf, width=2.5*sf, fill="gray")
        self.can.create_line(210*sf, 212.5*sf, 360*sf, 212.5*sf, width=2.5*sf, fill="gray")
        self.can.create_line(360*sf, 212.5*sf, 360*sf, 62.5*sf, width=2.5*sf, fill="gray")
        self.can.create_line(360*sf, 62.5*sf, 210*sf, 62.5*sf, width=2.5*sf, fill="gray")

        wrong_string = ""  # holds the string to print to screen
        for i in self.wrong_letters:  # for each letter in wrong_letters
            wrong_string += i  # add the letter
            wrong_string += " "  # plus a space

        self.can.create_text(285*sf, 72.5*sf, text=wrong_string[:8], font=self.font_size)  # add the first 8 characters of wrong string to the canvas

        if len(wrong_string) > 8:  # if the wrong string is longer the 8 characters
            self.can.create_text(285*sf, 92.5*sf, text=wrong_string[8:16], font=self.font_size)  # add the next 8 characters

        word_line = ""  # hold the string to print to string
        if self.current_word == "":  # if no word is selected
            word_line = "Please Select a List!"  # tell the user to select a list
        else:
            for i in self.current_word:  # for each letter in current word, set i to current one
                if i in self.correct_letters:  # if i is in correct letters, letter was already guessed
                    word_line += "{} ".format(i) # add i without
                    self.letters_left = self.letters_left - 1  # subtract 1 from letters left
                elif i == " ":  # if letter is a space
                    word_line += "{} ".format(i) # add a space
                    self.letters_left = self.letters_left - 1  # subtract 1 from letters left
                else:  # if letter hasnt been guessed
                    word_line += "__ "  # add an underscore

        if self.lives == 0:  # if user is out of lives
            self.gameEnabled = False  # disable game
            self.can.create_text(200*sf, 250*sf, text="You Lose! The Word Was '{}'!".format(self.current_word.title()), font=(self.font_size*4))  # display you lose and the word

        if self.letters_left == 0 and self.wrong_letters != "":  # if there are not letters left to guess
            self.gameEnabled = False  # disable the game
            self.can.create_text(200*sf, 250*sf, text="You Win!", font=(self.font_size*4))  # tell the user they are right

        self.can.create_text(200*sf, 275*sf, text=word_line, font=self.font_size)  # add the word line to the screen

        self.can.create_text(200*sf, 300*sf, text="Points: {}".format(self.score), font=self.font_size)  # add points to the screen
        self.can.create_text(200*sf, 325*sf, text="Guesses Left: {}".format(self.lives), font=self.font_size)  # add guesses left to the screen

    def check_guess(self, event):  # checks user input
        if self.gameEnabled == True: # if the game is enabled
            guess = str(self.guessBox.get())  # set guess to contents of entry box
            self.guessBox.delete(0, 'end')  # clear the entry box
            if len(guess) == 1 and guess.isalpha():  # verify the input is a single alphabetic character
                if guess in self.current_word and guess not in self.correct_letters:  # if input correct
                    self.correct_letters += guess  # append it to the correct letter list
                    self.score += 100  # increase score by 100
                elif guess not in self.current_word and guess not in self.wrong_letters:  # if guess is not correct
                    self.wrong_letters += guess  # append it to the wrong letter list
                    self.lives = self.lives - 1  # subtract a life
                    self.score -= 50  # remove 50 points

            elif guess == self.current_word:  # if the user entered the whole correct word
                self.correct_letters = self.current_word  # set correct letter to the current word
            else:  # if the user input is invalig
                pass  # self.check_guess(guess)  # call this method again

            # update the display
            self.draw_man(self.lives)
            self.update_letter_banks()
        else:
            pass # do nothing

    def change_list(self, event):  # changes the selected list
        choice = self.opt1_val.get()  # get the value of list selection box

        self.selected_list = "./lists/{}/".format(choice)  # store file path of selected list
        self.c.read("{}list.ini".format(self.selected_list))  # open the list.ini file
        self.selected_list += self.c.get('main', 'file')  # read the file name of the list, add it to selected_list var
        self.new_word() # generate a new word

    def new_word(self):  # randomly selects word from selected word list
        self.correct_letters = ""  # stores correct guesses
        self.wrong_letters = ""  # stores wrong guesses
        words = []  # stores all word from the list for easier manipulation
        list = open(self.selected_list, "rt")  # open list file for reading text
        for line in list:  # for each line (each word is on a new line)
            words.append(line.rstrip('\n')) # append the word to the word list
        list.close()  # close the list file
        self.current_word = (words[r.randint(0, len(words) - 1)]).lower()  # select random word from list and store it
        self.letters_left = len(self.current_word)  # store the length of the word

        self.lives = 6  # reset lives
        self.gameEnabled = True

        # update display
        self.draw_man(6)
        self.update_letter_banks()

g = Hangman_GUI() # create a new game instance


