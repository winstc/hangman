# Hangman

This is a basic hangman game built in python. The program chooses a random word from a list of words selected by the user. It then asks them to guess the word. There is both a 
Graphical version and a Text-Based version.


## Installation

1. Make sure you have [Python 3](https://www.python.org/downloads/) or higher installed on your system
2. Clone or Download this repository
3. Verify that hangman.py, hangman-GUI.py, config.ini and the 'lists' directory are in the same directory
4. Run hangman.py for Text mode or hangman_GUI.py for Graphical Mode

## Creating Word Lists

1. Create a new directory in the lists folder. This should be the name of the list.
2. In this folder make a list.ini file. This should follow the template provided in the tmplate folder
3. Write your words one per line in a file within the same folder. This file should have a .ls file extention
4. Check to make sure the file name in the .ini file is the same as the list file name
5. List shoud automaticly be found by hangman program

## Credits


Written By: Winston Cadwell