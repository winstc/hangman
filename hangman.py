# Written By: Winston Cadwell on 11/20/2016
# Project Name: Hangman Game
# Description: This is a command line hangman game. The user is presented with blank spaces and is
#              asked to guess the word. Word are loaded from lists in the "lists" directory.

from configparser import ConfigParser  # import configparser to read list configs
import random as r  # inport random library, used to select a random word
import os  # import os, used for function like clearing the screen


class Hangman:  # A class for all methods related to the hangman game
    def __init__(self):  # class init method
        self.selected_list = ""  # string to hold the users list choice
        self.current_word = ""  # string to hold the goal word
        self.correct_letters = ""  # string to store correct guesses
        self.wrong_letters = ""  # string to store wrong guesses
        self.lives = 6  # stores the number of lives the player has left
        self.letters_left = 0  # stores the number of letters the user still need to guess
        self.score = 0

    def start_game(self):  # selects a new word and starts the game, handles game flow
        self.lives = 6  # reset lives
        self.new_word()  # call new_word method to generate a new word
        while(self.lives > 0):  # run as long as the player has lives left
            self.refresh_disp()  # call refresh_disp to refresh the hangman display
            self.take_guess()  # call take_guess so the user can make a guess

        # when user runs out of lives and the loop finishes
        self.clear()  # clear the screen
        print("You failed to guess the word correctly!")  # tell the user they did not guess the word right
        print("The word was {}".format(self.current_word))  # tell the user what the word was
        self.play_again()  # call play_again method

    def clear(self):  # method for clearing the screen
        os.system('clear')  # clear the screen

    def choose_list(self):  # allows user to choose which word list to use
        list_dict = {}  # create a dictionary for storing available word lists
        c = ConfigParser()  # create a new config parser called 'c'
        count = 1  # counter variable used to index lists
        self.clear()  # clear screen
        for foldername in os.listdir('./lists'):  # for each folder in the list directory
            c.read("./lists/{}/list.ini".format(foldername))  # open its list.ini file with config parser
            print(count, ".", c.get('main', 'name').title())  # from section main read name, show it to the player along with countr index
            print("\tCategory:", c.get('main', 'category'))  # from section main read category, show it to the player
            print("\tDifficulty:", c.get('main', 'difficulty'))  # from main read the difficulty, show it to the player
            list_dict[count] = str(c.get('main', 'name'))  # add the list name to list dictionary
            print("")  # print a space to get a new line
            count += 1  # increment counter by 1

        choice = input("Please choose a list.\n>>> ") # ask the user to choose a list and store their choice
        try:
            choice = int(choice)  # try to convert input to int
        except ValueError:  # if it doesn't work
            self.choose_list()  # start over

        if choice in list_dict:  # if user choice is in the list dictionary
            self.selected_list = "./lists/{}/".format(list_dict[choice])  # store file path of selected list
            c.read("{}list.ini".format(self.selected_list))  # open the list.ini file
            self.selected_list += c.get('main', 'file')  # read the file name of the list
        else:  # if the user enters an invalid option
            self.choose_list()  # call choose_list again

    def new_word(self):  # randomly selects word from selected word list
        self.correct_letters = ""  # stores correct guesses
        self.wrong_letters = ""  # stores wrong guesses
        words = []  # stores all word from the list for easier manipulation
        list = open(self.selected_list, "rt")  # open list file for reading text
        for line in list:  # for each line (each word is on a new line)
            words.append(line.rstrip('\n')) # append the word to the word list
        list.close()  # close the list file
        self.current_word = (words[r.randint(0, len(words) - 1)]).lower()  # select random word from list and store it
        self.letters_left = len(self.current_word)  # store the length of the word

    def refresh_disp(self):  # refreshes hangman display
        self.clear()  # clear the screen

        print("Points: {}".format(self.score))

        # The folowing block of if statements draw the hangman graphic
        if (self.lives == 6):  # if the user has 6 lives
            # draw according frame
            print(" -------\n"
                  " |     |\n"
                  " |      \n"
                  " |\n"
                  "_|_\n")
        elif (self.lives == 5):  # if the user has 5 lives
            # draw according frame
            print(" -------\n"
                  " |     |\n"
                  " |     O\n"
                  " |\n"
                  "_|_\n")
        elif (self.lives == 4):  # if the user has 4 lives
            # draw the according frame
            print(" -------\n"
                  " |     |\n"
                  " |     O\n"
                  " |     |\n"
                  "_|_\n")
        elif (self.lives == 3):  # if the user has 3 lives
            # draw the according frame
            print(" -------\n"
                  " |     |\n"
                  " |     O\n"
                  " |    /|\n"
                  "_|_\n")
        elif (self.lives == 2):  # if the user has 2 lives
            # draw the according frame
            print(" -------\n"
                  " |     |\n"
                  " |     O\n"
                  " |    /|\ \n"
                  "_|_\n")
        elif (self.lives == 1):  # if the user has 1 life
            # draw the according frame
            print(" -------\n"
                  " |     |\n"
                  " |     O\n"
                  " |    /|\ \n"
                  "_|_   /\n")
        elif (self.lives == 0): # if the user has no lives left
            # draw the according frame
            print(" -------\n"
                  " |     |\n"
                  " |     O\n"
                  " |    /|\ \n"
                  "_|_   / \ \n")

        self.letters_left = len(self.current_word)  # set letters left to length of current word

        for i in self.current_word:  # for each letter in current word, set i to current one
            if i in self.correct_letters:  # if i is in correct letters, letter was already guessed
                print(i , end=" ")  # print i without new line
                self.letters_left = self.letters_left - 1  # subtract 1 from letters left
            elif i == " ":  # if letter is a space
                print(i, end=" ")  # print a spcae
                self.letters_left = self.letters_left -1  # subtract 1 from letters left
            else:  # if letter hasnt been guessed
                print("__", end=" ")  # print blank space

        print("")  # print a blank line

        print("-----------------------------------")  # print a divider line
        print("Incorrect Letters:")
        for i in self.wrong_letters:  # for each wrong letter
            print(i, end=" ")  # print the letter to the screen

        print("")  # print a blank line
        print("You have {} guesses left!".format(self.lives))
        # checks if there are no letters left
        # if they don't it means the word is correct
        if self.letters_left == 0:  # if there are not letters left to guess
            print("You guessed the word correct!")  # tell the user they got the word right
            self.play_again()  # goto play again function

    def take_guess(self):  # handles collecting input from user
        guess = input("Guess a letter or the word \n>>> ").lower()  # print promt for the user
        if len(guess) == 1 and guess.isalpha():  # verify the input is a single alphabetic character
            if guess in self.current_word and guess not in self.correct_letters: # if input correct
                self.correct_letters += guess  # append it to the correct letter list
                self.score += 100
            elif guess not in self.current_word and guess not in self.wrong_letters:  # if guess is not correct
                self.wrong_letters += guess  # append it to the wrong letter list
                self.lives = self.lives - 1  # subtract a life
                self.score -= 50
            elif guess in self.correct_letters or guess in self.wrong_letters:  # if guess is already in a list
                print("You already guessed that!")  # tell the user they already guessed it
                self.take_guess()  # call take_guess again
        elif guess == self.current_word:  # if the user entered the whole correct word
            self.correct_letters = self.current_word  # set correct letter to the current word
        else:  # if the user input is invalig
            self.take_guess()  # call this method again

    # asks the user if they want to play again
    def play_again(self):
        if input("Would you like to play again?").lower() == 'y':  # ask the user if they want to play again, if yes
            if input("Do you want to choose a new list?").lower() == 'y':  # ask if they want a new list
                self.choose_list()  # call choose list function
            self.start_game()  # start game again
        else:  # if the user answers something else
            self.clear()  # clear the screen
            print("Thanks for playing!")  # Thank the user for playing
            exit(1)  # exit the program



h = Hangman()  # create new instance of hangman called h
h.choose_list()  # call method to pick a word list
h.start_game()  # start a new game
